
# Fælles projekt i 20U

Semesterprøven fra januar 2021 løses i fællesskab af alle i 20U.

Herunder er opstillet en række delopgaver. Hver delopgave har et eller to navne på studerende, som skal løse opgaven.

Hver studerende modtager en invitation til dette repository, som accepteres. Derefter opretter den studerende en lokal kopi af dette repo.

**Al kode skrive i det lokale repo** (det er forbudt at skrive kode direkte i repo'et på Bitbucket). Koden skrives lettest direkte i master branchen, hvilket er OK, da dette er et meget kortlivet repo. (Det er altså ikke nødvendigt at oprette en feature branch på i det lokale repo til ny-udviklet kode, men det er tilladt.)

Hjælp til koden kan fås ved at skrive en kommentar i koden med indholdet `// Need help`. Så kan en anden studerende tilføje kommentarer eller kode, som kan hjælpe.

Hvis en studerende i stedet for en løsning på delopgaven skriver en kommentar i koden med indholdet `// Need code`, så overtager en anden studerende frivilligt delopgaven (men den første studerende får dog vist, at han/hun kan bruge GIT).

**Alle studerende må kommentere kode, som en anden studerende har skrevet. Det er så op til den anden studerende at afvise kommentaren eller rette koden til.**

*Når du skriver kode (eller kommentar), så tilføj en kommentar med dit navn.*  
OBS: Brug ikke æ,ø eller å i koden.

Delopgaver:

- Opg. S0: Java pakker med tomme klasser  
Programmør: Mikael (mlch)

- Opg. S1: Klassen Fag med link (link på den nye måde fra 2. sem.)  
Programmør: Ahmed

- Opg. S1: Klassen Lektion med link (link på den nye måde fra 2. sem.)  
Programmør: Alexander

- Opg. S1: Klassen Deltagelse med link (link på den nye måde fra 2. sem.)  
Programmør: Anders L.

- Opg. S1: Klassen Studerende med link (link på den nye måde fra 2. sem.)  
Programmør: Anders F.T.

- Opg. S1: Klassen Lektion med link (link på den nye måde fra 2. sem.)  
Programmør: Andreas

- Opg. S2: Metoden erRegistreretFravaerende() i klassen Deltagelse  
Programmør: Camilla T.

- Opg. S3: Metoden antalFravaersLektioner() i klassen Studerende  
Programmør: Camilla B.T.

- Opg. S4: Metoden sygdomPaaDato() i klassen Fag  
Programmør: Casper

- Opg. S5: Koden i klassen Storage  
Programmør: Cecilie

- Opg. S6: Opret metoder og metoden initStorage() i klassen Controller  
Programmør: Christian B.

- Opg. S7: Metoden opretDeltagelser() i klassen Deltagelse og brug i initStorage()  
Programmør: Christian L.J.

- Opg. S8: Metoden StuderendeTilObservation() i klassen Controller  
Programmør: Laura og Jacob

- Opg. S9: Metoden fravaerOmgaengere() i klassen Controller  
Programmør: Jimmi og Jonas

- Opg. S10: Metoden mestFravaerLektion() i klassen Fag  
Programmør: Mads og Mathias

- Opg. S11: Metoden initContent() i klassen MainApp, men kun udseende af de 3 lister med labels (resten løses i delopgaverne herunder)  
Programmør: Oliver

- Opg. S11: Metoden initContent() i klassen MainApp, men kun udseende af radiobuttons med label (resten løses i delopgaverne herunder)  
Programmør: Rune

- Opg. S11: Metoden fagSelectionChanged() i klassen MainApp med nødvendig kode i initContent() (`ListView<Fag>`)  
Programmør: Sakariye

- Opg. S11: Metoden lektionSelectionChanged() i klassen MainApp med nødvendig kode i initContent() (`ListView<Lektion>`)  
Programmør: Thomas B.M.

- Opg. S11: Metoden fravaerAction() i klassen MainApp med nødvendig kode i initContent() (`Button("Fravær")`)  
Programmør: Thomas T.P.

Bemærk, at nogle delopgaver forudsætter, at andre delopgaver allerede er løst.
