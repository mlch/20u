package storage;

import java.util.ArrayList;

import model.Fag;
import model.Studerende;

public abstract class Storage {

	private static ArrayList<Fag> fag = new ArrayList<>();
	private static ArrayList<Studerende> studerende = new ArrayList<>();

	public static void storeFag(Fag f) {
		fag.add(f);
	}

	public static void storeStuderende(Studerende s) {
		studerende.add(s);
	}

	public static ArrayList<Fag> getFag() {
		return new ArrayList<>(fag);
	}

	public static ArrayList<Studerende> getStuderende() {
		return new ArrayList<>(studerende);
	}
}
