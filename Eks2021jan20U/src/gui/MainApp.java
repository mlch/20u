package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import controller.Controller;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Deltagelse;
import model.DeltagerStatus;
import model.Fag;
import model.Lektion;

public class MainApp extends Application {
	
	
	Label lbl_fag = new Label("Fag");
	Label lbl_lektion = new Label("Lektion");
	Label lbl_deltagelse = new Label("Deltagelse");
	
	ListView<Fag> lw_fag = new ListView<Fag>();
	ListView<Lektion> lw_lektion = new ListView<Lektion>();
	ListView<Deltagelse> lw_deltagelse = new ListView<Deltagelse>();
	

    @Override
    public void init() throws Exception {

    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Fraværssystem");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }
    
    private final ToggleGroup group = new ToggleGroup();
    
    
    private void initContent(GridPane pane) {
    	
    	pane.setPadding(new Insets(20));
    	pane.setVgap(15);
    	pane.setHgap(15);
    	
    	//ListView
    	lw_fag.setMaxHeight(200);
    	lw_lektion.setMaxHeight(200);
    	lw_deltagelse.setMaxHeight(200);
// -----------------------TEST CODE-----------------------------------------------------------------------------
    	ArrayList list = new ArrayList<Fag>();
    	list.add(new Fag("DataNAVN" , "DataKLASSE"));
    	list.add(new Fag("DataNAVN" , "DataKLASSE"));
    	list.add(new Fag("DataNAVN" , "DataKLASSE"));
    	
    	ArrayList list2 = new ArrayList<Lektion>();
    	list2.add(new Lektion(LocalDate.of(2021, 01, 01) , LocalDate.of(2021, 01, 02), "20A"));
    	list2.add(new Lektion(LocalDate.of(2021, 02, 02) , LocalDate.of(2021, 02, 03), "20B"));
    	list2.add(new Lektion(LocalDate.of(2021, 03, 03) , LocalDate.of(2021, 04, 04), "20C"));

    	ArrayList list3 = new ArrayList<Deltagelse>();
    	list3.add(new Deltagelse(true, DeltagerStatus.TILSTEDE,null ,null));
    	list3.add(new Deltagelse(false, DeltagerStatus.FRAVAER,null ,null));
    	list3.add(new Deltagelse(false, DeltagerStatus.SYG,null ,null));
    	
    	lw_fag.getItems().setAll(list);
    	lw_lektion.getItems().setAll(list2);
    	lw_deltagelse.getItems().setAll(list3);
// -----------------------ORIGIN CODE-----------------------------------------------------------------------------

//    	lw_fag.getItems().setAll(Controller.getFag());
//    	lw_lektion.getItems().setAll(Controller.getLektioner());
//    	lw_deltagelse.getItems().setAll(Controller.getDeltagelser());
    	
    	//PaneAdd
    	pane.add(lbl_fag, 0, 0);
    	pane.add(lbl_lektion, 1, 0);
    	pane.add(lbl_deltagelse, 2, 0);
    	
    	pane.add(lw_fag, 0, 1);
    	pane.add(lw_lektion, 1, 1);
    	pane.add(lw_deltagelse, 2, 1);
    	  	
    	Label lblRbTitle = new Label("Fraværsårsager");
    	pane.add(lblRbTitle,4,0);
    	
    	VBox box = new VBox();
    	pane.add(box, 4, 1);
    	
    	RadioButton rbTilstede = new RadioButton();
    	rbTilstede.setToggleGroup(group);
    	rbTilstede.setText("Tilstede");
    	rbTilstede.setUserData(DeltagerStatus.TILSTEDE);
    	box.getChildren().add(rbTilstede);
    	
    	RadioButton rbFravær = new RadioButton();
    	rbFravær.setToggleGroup(group);
    	rbFravær.setText("Fravær");
    	rbFravær.setUserData(DeltagerStatus.FRAVAER);
    	box.getChildren().add(rbFravær);
    	
    	RadioButton rbSyg = new RadioButton();
    	rbSyg.setToggleGroup(group);
    	rbSyg.setText("Syg");
    	rbSyg.setUserData(DeltagerStatus.SYG);
    	box.getChildren().add(rbSyg);
    	
    	RadioButton rbAfbud = new RadioButton();
    	rbAfbud.setToggleGroup(group);
    	rbAfbud.setText("Afbud");
    	rbAfbud.setUserData(DeltagerStatus.AFBUD);
    	box.getChildren().add(rbAfbud);
    	
    	
    	
    	
    	
    }
}
