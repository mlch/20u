package model;

public class Deltagelse {
    private boolean registreret;
    private DeltagerStatus status;

    private Studerende studerende;
    private Lektion lektion;

    public Deltagelse(boolean registreret, DeltagerStatus status, Studerende studerende,
            Lektion lektion) {
        this.registreret = registreret;
        this.status = status;
        this.studerende = studerende;
        this.lektion = lektion;
    }
    //Opg1

    public void setStuderende(Studerende studerende) {
        this.studerende = studerende;
    }

    public Studerende getStuderende() {
        return this.studerende;
    }

    public void setLektion(Lektion lektion) {
        this.lektion = lektion;
    }

    /*
     * Lidt usikker om det her er den nye måde at remove
     *
     */
    public void remLektion() {
        this.setLektion(null);
    }

    public void remStuderende() {
        this.setStuderende(null);
    }

    //Camilla BT - skulle bruges i forbindelse m. Opg.S3
    public DeltagerStatus getStatus() {
        return status;
    }

    // opg 2:
    public boolean erRegistreretFraværende() {
        boolean erFraværende = false;
        if (registreret == true && !status.equals(DeltagerStatus.TILSTEDE)) {
            erFraværende = true;
        }
        return erFraværende;
    }

    @Override
    public String toString() {
        return registreret + " " + status;
    }

}
