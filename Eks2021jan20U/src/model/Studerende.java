package model;

import java.util.ArrayList;

public class Studerende {

    private String navn;
    private String email;
    private final ArrayList<Deltagelse> deltagelser = new ArrayList<Deltagelse>();

    public Studerende(String navn, String email) {
        this.navn = navn;
        this.email = email;
    }

    public String getNavn() {
        return navn;
    }

    public String getEmail() {
        return email;
    }

    public ArrayList<Deltagelse> getDeltagelser() {
        return new ArrayList<>(deltagelser);
    }

    public void addDeltagelse(Deltagelse deltagelse) {
        if (!deltagelser.contains(deltagelse)) {
            deltagelser.add(deltagelse);
            deltagelse.setStuderende(this);

        }
    }

    public void removeDeltagelse(Deltagelse deltagelse) {
        if (deltagelser.contains(deltagelse)) {
            deltagelser.remove(deltagelse);
            deltagelse.setStuderende(null);
        }
    }

    public void add(Studerende studerende) {

    }

    //Camilla BT - Opg.S3
    public int antalFravaersLektioner() {
        int antalFravaersLektioner = 0;

        for (Deltagelse d : deltagelser) {
            if (d.getStatus() != DeltagerStatus.TILSTEDE) {
                antalFravaersLektioner++;
            }
        }

        return antalFravaersLektioner;
    }

}
