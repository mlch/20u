package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Fag {

	private String navn;
	private String klasse;

	private final ArrayList<Lektion> lektioner = new ArrayList<>();

	public Fag(String navn, String klasse) {
		this.navn = navn;
		this.klasse = klasse;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getKlasse() {
		return klasse;
	}

	public void setKlasse(String klasse) {
		this.klasse = klasse;
	}

	public ArrayList<Lektion> getLektioner() {
		// Kunnde måske give arraylisten en type <lektion> -csm
		return new ArrayList(lektioner);
	}

	public void addLektion(Lektion lektion) {
		lektioner.add(lektion);
	}

	public void removeLektion(Lektion lektion) {
		lektioner.remove(lektion);

	}

	// opgave s4 -csm
	public ArrayList<Studerende> sygdomPåDato(LocalDate Dato) {
		for (int i = 0; i < lektioner.size(); i++) {
			// TODO når lektions klassen er færdig :D
		}
		return null;
	}

	// Opgave S10 - Mads Bang Kristensen
	public Lektion lektionMedMestFravær(ArrayList<Lektion> list, Lektion target) {
		Lektion mestFravær = null;
		int indeks = -1;
		int i = 0;
		while (indeks == -1 && i < list.size()) {
			mestFravær = list.get(i);
			if (mestFravær.equals(target))
				indeks = i;
			else
				i++;
		}
		return mestFravær;
	}

	@Override
	public String toString() {
	return navn + " " + klasse;
	}
	
}
