package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import model.Deltagelse;
import model.Studerende;
import storage.Storage;

public abstract class Controller {

	
    public static void initStorage() {

        //S7: Christian Lynggaard J�rgensen
        opretDeltagelser(pro1S20, peter);
        opretDeltagelser(pro1S20, tina);
        opretDeltagelser(pro1S20, sascha);
    }

    //Opgave S8 studerende til observation, Jakob Fejerskov
 // Hjælpemetode til opgave S8
    private static void insertionSort(ArrayList<Studerende> arr) {
        for (int i = 1; i < arr.size(); i++) {
            Studerende temp = arr.get(i);
            int j = i;
            while (j > 0 && temp.compareTo(arr.get(j - 1)) < 0) {
                arr.set(j, arr.get(j - 1));
                j--;
            }
            arr.set(j, temp);
        }
    }

    // Opgave S8
    public static ArrayList<Studerende> studerendeTilObservation(int grænse) {
        ArrayList<Studerende> studerendeTilObservation = new ArrayList<>();
        int antalFraværsLektioner = 0;
        for (Studerende studerende : Storage.getStuderende()) {
            for (Deltagelse deltagelse : studerende.getDeltagelser()) {
                if (deltagelse.erRegistreretFraværende() == true) {
                    antalFraværsLektioner++;
                }
            }
            if (antalFraværsLektioner >= grænse) {
                studerendeTilObservation.add(studerende);
            }
        }
        insertionSort(studerendeTilObservation);
        return studerendeTilObservation;
    }
    
    //Jonas Sillesen, fraværomgængere
    public static void fraværOmgængere(String[] omgængere, ArrayList<Studerende> megetFravær,
            String filnavn) {
        File out = new File(filnavn);
        try (PrintWriter writer = new PrintWriter(out)) {
            int i1 = 0;
            int i2 = 0;
            // flet sålænge der er noget i begge lister
            while (i1 < megetFravær.size() && i2 < omgængere.length) {
                if (megetFravær.get(i1).toString().compareTo(omgængere[i2]) < 0) {
                    // l1's første omgænger er mindst
                    i1++;
                } else if (omgængere[i2].compareTo(megetFravær.get(i1).toString()) < 0) {
                    // s2's første omgænger er mindst
                    i2++;
                } else {
                    //omgængere er det samme
                    writer.println(megetFravær.get(i1).getNavn());
                    i1++;
                    i2++;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e.getMessage());
        } catch (SecurityException e) {
            System.out.println("Security violation: " + e.getMessage());
        }
    }
    //Opgave S7: Christian Lynggaard J�rgensen
    public static void opretDeltagelser(Fag fag, Studerende studerende) {
        for (Lektion lektion : fag.getLektioner()) {
            Deltagelse delt = new Deltagelse(false, TILSTEDE, studerende, lektion);
            lektion.addDeltagelse(deltagelse);
            studerende.addDeltagelse(deltagelse);
        }
    }
}
