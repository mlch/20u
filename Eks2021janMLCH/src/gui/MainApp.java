package gui;

import controller.Controller;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Deltagelse;
import model.DeltagerStatus;
import model.Fag;
import model.Lektion;
import storage.Storage;

public class MainApp extends Application {

    @Override
    public void init() {
        Controller.initStorage();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Fraværssystem");
        GridPane pane = new GridPane();
        this.initContent(pane);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    // -------------------------------------------------------------------------

    private final ListView<Fag> lvwFag = new ListView<>();
    private final ListView<Lektion> lvwLektion = new ListView<>();
    private final ListView<Deltagelse> lvwDeltagelse = new ListView<>();
    private final ToggleGroup group = new ToggleGroup();

    public void initContent(GridPane pane) {
        pane.setPadding(new Insets(20.0));
        pane.setHgap(20.0);
        pane.setVgap(10.0);
        pane.setGridLinesVisible(false);

        Label lblFag = new Label("Fag");
        pane.add(lblFag, 0, 0);

        pane.add(lvwFag, 0, 1);
        lvwFag.setPrefHeight(200.0);
        lvwFag.setPrefWidth(250.0);
        lvwFag.getItems().addAll(Storage.getAlleFag());

        ChangeListener<Fag> listener = (ov, o, n) -> this.fagSelectionChanged();
        lvwFag.getSelectionModel().selectedItemProperty().addListener(listener);

        Label lblLektion = new Label("Lektion");
        pane.add(lblLektion, 1, 0);

        pane.add(lvwLektion, 1, 1);
        lvwLektion.setPrefHeight(200.0);
        lvwLektion.setPrefWidth(250.0);

        ChangeListener<Lektion> listener1 = (ov, o, n) -> this.lektionSelectionChanged();
        lvwLektion.getSelectionModel().selectedItemProperty().addListener(listener1);

        Label lblDeltagelse = new Label("Deltagelse");
        pane.add(lblDeltagelse, 2, 0);

        pane.add(lvwDeltagelse, 2, 1);

        Label lblFravaersaarsag = new Label("Fraværsårsag");
        pane.add(lblFravaersaarsag, 3, 0);

        lvwDeltagelse.setPrefHeight(200.0);
        lvwDeltagelse.setPrefWidth(250.0);

        VBox vBox = new VBox(10.0);
        pane.add(vBox, 3, 1);

        for (DeltagerStatus status : DeltagerStatus.values()) {
            RadioButton rbn = new RadioButton(status.toString());
            vBox.getChildren().add(rbn);
            rbn.setToggleGroup(group);
        }
        group.selectToggle(group.getToggles().get(0));

        Button btnFravaer = new Button("Fravær");
        vBox.getChildren().add(btnFravaer);
        btnFravaer.setOnAction(event -> fravaerAction());
    }

    // -------------------------------------------------------------------------

    private void fagSelectionChanged() {
        Fag fag = lvwFag.getSelectionModel().getSelectedItem();
        if (fag != null) {
            lvwLektion.getItems().setAll(fag.getLektioner());
            lvwDeltagelse.getItems().clear();
        }
    }

    private void lektionSelectionChanged() {
        Lektion lektion = lvwLektion.getSelectionModel().getSelectedItem();
        if (lektion != null) {
            lvwDeltagelse.getItems().setAll(lektion.getDeltagelser());
        }
    }

    private void fravaerAction() {
        Lektion lektion = lvwLektion.getSelectionModel().getSelectedItem();
        Deltagelse deltagelse = lvwDeltagelse.getSelectionModel().getSelectedItem();
        RadioButton rbn = (RadioButton) group.getSelectedToggle();
        DeltagerStatus status = DeltagerStatus.valueOf(rbn.getText());
        if (lektion != null && deltagelse != null) {
            Controller.updateFravaer(deltagelse, status);
            lvwDeltagelse.getItems().setAll(lektion.getDeltagelser());
        }
    }
}
