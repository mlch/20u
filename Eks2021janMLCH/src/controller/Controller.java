package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

import model.Deltagelse;
import model.DeltagerStatus;
import model.Fag;
import model.Lektion;
import model.Studerende;
import storage.Storage;

// S6 (5 p)
public class Controller {

    public static Studerende opretStuderende(String navn, String email) {
        Studerende studerende = new Studerende(navn, email);
        Storage.addStuderende(studerende);
        return studerende;
    }

    public static Fag opretFag(String navn, String klasse) {
        Fag fag = new Fag(navn, klasse);
        Storage.addFag(fag);
        return fag;
    }

    public static Lektion opretLektion(LocalDate dato, LocalTime startTid, String lokale, Fag fag) {
        var lektion = new Lektion(dato, startTid, lokale);
        fag.addLektion(lektion);
        return lektion;
    }

    public static void initStorage() {
        var peter = opretStuderende("Peter Hansen", "ph@stud.dk");
        var tina = opretStuderende("Tina Jensen", "tj@stud.dk");
        var sascha = opretStuderende("Sascha Petersen", "sp@stud.dk");

        var pro1S20 = opretFag("PRO1", "20S");
        opretFag("PRO1", "20T");
        opretFag("SU1", "20S");

        opretLektion(LocalDate.parse("2021-02-01"), LocalTime.parse("08:30"), "A1.32", pro1S20);
        opretLektion(LocalDate.parse("2021-02-01"), LocalTime.parse("10:30"), "A1.32", pro1S20);
        opretLektion(LocalDate.parse("2021-02-03"), LocalTime.parse("08:30"), "A1.32", pro1S20);
        opretLektion(LocalDate.parse("2021-02-03"), LocalTime.parse("10:30"), "A1.32", pro1S20);

        // Part of S7 (7 p)
        opretDeltagelser(pro1S20, peter);
        opretDeltagelser(pro1S20, tina);
        opretDeltagelser(pro1S20, sascha);
    }

    // Part of S7 (7 p)
    public static void opretDeltagelser(Fag fag, Studerende studerende) {
        for (var lektion : fag.getLektioner()) {
            var deltagelse = new Deltagelse(studerende, lektion);
            lektion.addDeltagelse(deltagelse);
            studerende.addDeltagelse(deltagelse);
        }
    }

    // Part of S8 (10 p)
    // Other part in Studerende.compareTo()
    // not used
    public static ArrayList<Studerende> studerendeTilObservation(int graense) {
        ArrayList<Studerende> studerendeListe = new ArrayList<Studerende>();
        for (Studerende studerende : Storage.getAlleStuderende()) {
            if (studerende.antalFravaersLektioner() >= graense) {
                studerendeListe.add(studerende);
            }
        }
        Collections.sort(studerendeListe);
        return studerendeListe;
    }

    // S9 (10 p)
    // not used
    public static void fravaerOmgaengere(
            String[] omgaengere, ArrayList<Studerende> megetFravaer, String filnavn) {
        ArrayList<Studerende> studerendeListe = new ArrayList<Studerende>();
        int i1 = 0;
        int i2 = 0;
        while (i1 < omgaengere.length && i2 < megetFravaer.size()) {
            if (omgaengere[i1].compareTo(megetFravaer.get(i2).getNavn()) < 0) {
                i1++;
            } else if (omgaengere[i1].compareTo(megetFravaer.get(i2).getNavn()) > 0) {
                i2++;
            } else {
                studerendeListe.add(megetFravaer.get(i2));
                i1++;
                i2++;
            }
        }

        File out = new File(filnavn);
        try (PrintWriter writer = new PrintWriter(out)) {
            for (Studerende studerende : studerendeListe) {
                writer.println(studerende.getNavn() + " " + studerende.antalFravaersLektioner());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Fejl: $ex");
        }
    }

    // Used in MainApp.fravaerAction()
    // Part of S11 (20 p)
    public static void updateFravaer(Deltagelse deltagelse, DeltagerStatus status) {
        deltagelse.setRegistreret(true);
        deltagelse.setStatus(status);
    }
}
