package model;

public class Deltagelse {
    private boolean registreret;
    private DeltagerStatus status;

    // association 0..* -> 1 Studerende
    private Studerende studerende;
    // composition 0..* -> 1 Lektion
    private final Lektion lektion;

    public Deltagelse(Studerende studerende, Lektion lektion) {
        this.studerende = studerende;
        this.lektion = lektion;
        registreret = false;
        status = DeltagerStatus.TILSTEDE;
    }

    // not used
    public boolean isRegistreret() {
        return registreret;
    }

    // Used in Controller.updateFravaer()
    public void setRegistreret(boolean registreret) {
        this.registreret = registreret;
    }

    public DeltagerStatus getStatus() {
        return status;
    }

    // Used in Controller.updateFravaer()
    public void setStatus(DeltagerStatus status) {
        this.status = status;
    }

    // Used in GUI
    @Override
    public String toString() {
        return studerende.getNavn() + ", " + registreret + ", " + status;
    }

    //-----------------------------------------------------

    // not used
    public Lektion getLektion() {
        return lektion;
    }

    public Studerende getStuderende() {
        return studerende;
    }

    // not used
    public void setStuderende(Studerende studerende) {
        this.studerende = studerende;
    }

    //-----------------------------------------------------

    // S2 (3 p)
    public boolean erRegistreretFravaerende() {
        return registreret && status != DeltagerStatus.TILSTEDE;
    }
}
