package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Fag {
    private String navn;
    private String klasse;

    // association 1 --> 0..* Lektion
    private final ArrayList<Lektion> lektioner = new ArrayList<>();

    public Fag(String navn, String klasse) {
        this.navn = navn;
        this.klasse = klasse;
    }

    // not used
    public String getNavn() {
        return navn;
    }

    // not used
    public String getKlasse() {
        return klasse;
    }

    // Used in GUI
    @Override
    public String toString() {
        return navn + " " + klasse;
    }

    //-----------------------------------------------------

    public ArrayList<Lektion> getLektioner() {
        return new ArrayList<>(lektioner);
    }

    public void addLektion(Lektion lektion) {
        lektioner.add(lektion);
    }

    // not used
    public void removeLektion(Lektion lektion) {
        lektioner.remove(lektion);
    }

    //-----------------------------------------------------

    // S4 (8 p)
    // not used
    public ArrayList<Studerende> sygdomPaaDato(LocalDate dato) {
        ArrayList<Studerende> syge = new ArrayList<>();
        for (Lektion lektion : lektioner) {
            if (lektion.getDato().equals(dato)) {
                for (Deltagelse deltagelse : lektion.getDeltagelser()) {
                    if (deltagelse.getStatus() == DeltagerStatus.SYG &&
                            !syge.contains(deltagelse.getStuderende())) {
                        syge.add(deltagelse.getStuderende());
                    }
                }
            }
        }
        return syge;
    }

    // S10 (10 p)
    // not used
    public Lektion mestFravær() {
        Lektion maxLektion = null;
        int maxFravaer = 0;
        for (Lektion lektion : lektioner) {
            int antal = 0;
            for (Deltagelse deltagelse : lektion.getDeltagelser()) {
                if (deltagelse.erRegistreretFravaerende()) {
                    antal++;
                }
            }
            if (antal > maxFravaer) {
                maxLektion = lektion;
                maxFravaer = antal;
            }
        }
        return maxLektion;
    }

    // Part of S10 (10 p)
    // Part in Lektion.antalFravaerendeStuderende()
    // not used
    public Lektion mestFravaer() {
        Lektion maxLektion = null;
        var maxFravaer = 0;
        for (Lektion lektion : lektioner) {
            if (lektion.antalFravaerendeStuderende() > maxFravaer) {
                maxLektion = lektion;
                maxFravaer = lektion.antalFravaerendeStuderende();
            }
        }
        return maxLektion;
    }
}
