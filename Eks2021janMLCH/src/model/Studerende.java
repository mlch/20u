package model;

import java.util.ArrayList;

public class Studerende implements Comparable<Studerende> { // Part of S8
    private String navn;
    private String email;

    // association 1 -> 0..* Deltagelse
    private final ArrayList<Deltagelse> deltagelser = new ArrayList<>();

    public Studerende(String navn, String email) {
        this.navn = navn;
        this.email = email;
    }

    public String getNavn() {
        return navn;
    }

    // not used
    public String getEmail() {
        return email;
    }

    //-----------------------------------------------------

    // not used
    public ArrayList<Deltagelse> getDeltagelser() {
        return new ArrayList<>(deltagelser);
    }

    public void addDeltagelse(Deltagelse deltagelse) {
        deltagelser.add(deltagelse);
    }

    // not used
    public void removeDeltagelse(Deltagelse deltagelse) {
        deltagelser.add(deltagelse);
    }

    //-----------------------------------------------------

    // S3 (4 p)
    public int antalFravaersLektioner() {
        int antalFravaer = 0;
        for (Deltagelse deltagelse : deltagelser) {
            if (deltagelse.erRegistreretFravaerende()) {
                antalFravaer++;
            }
        }
        return antalFravaer;
    }

    // Part of S8 (10 p)
    @Override
    public int compareTo(Studerende other) {
        return this.navn.compareTo(other.navn);
    }
}
