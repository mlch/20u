package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Lektion {
    private LocalDate dato;
    private LocalTime startTid;
    private String lokale;

    // composition 1 -> 0..* Deltagelse
    private final ArrayList<Deltagelse> deltagelser = new ArrayList<>();

    public Lektion(LocalDate dato, LocalTime startTid, String lokale) {
        this.dato = dato;
        this.startTid = startTid;
        this.lokale = lokale;
    }

    public LocalDate getDato() {
        return dato;
    }

    // not used
    public LocalTime getStartTid() {
        return startTid;
    }

    // not used
    public String getLokale() {
        return lokale;
    }

    // Used in GUI
    @Override
    public String toString() {
        return dato + " " + startTid + " " + lokale;
    }

    //-----------------------------------------------------

    public ArrayList<Deltagelse> getDeltagelser() {
        return new ArrayList<>(deltagelser);
    }

    public void addDeltagelse(Deltagelse deltagelse) {
        deltagelser.add(deltagelse);
    }

    //-----------------------------------------------------

    // Part of S10 (10 p)
    public int antalFravaerendeStuderende() {
        var antalFravaer = 0;
        for (Deltagelse deltagelse : deltagelser) {
            if (deltagelse.erRegistreretFravaerende()) {
                antalFravaer++;
            }
        }
        return antalFravaer;
    }
}
