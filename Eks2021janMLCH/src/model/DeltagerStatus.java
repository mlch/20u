package model;

public enum DeltagerStatus {
    TILSTEDE, FRAVAER, SYG, AFBUD
}
