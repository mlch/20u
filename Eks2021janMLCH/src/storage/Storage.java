package storage;

import java.util.ArrayList;

import model.Fag;
import model.Studerende;

public abstract class Storage {
    private static ArrayList<Fag> alleFag = new ArrayList<>();
    private static ArrayList<Studerende> alleStuderende = new ArrayList<>();

    public static ArrayList<Fag> getAlleFag() {
        return new ArrayList<>(alleFag);
    }

    public static ArrayList<Studerende> getAlleStuderende() {
        return new ArrayList<>(alleStuderende);
    }

    public static void addFag(Fag fag) {
        alleFag.add(fag);
    }

    public static void addStuderende(Studerende studerende) {
        alleStuderende.add(studerende);
    }
}
